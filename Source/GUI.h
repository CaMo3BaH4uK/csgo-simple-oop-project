#pragma once

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>
#include <GLFW/glfw3.h>
#include <Windows.h>
#include <utility>

class GUI {
public:
	GUI();

	void Prepare();
	void Render();
	void SetWindowPos(RECT lnPos);
	void SetWindowSize(std::pair<int, int> lnSize);
	void SetWindowMinimization(bool lnMinimize);
	void SetVisible(bool lnVisible);
	bool IsMinimized();
	bool IsVisible();
	bool WindowShouldClose();
	GLFWwindow* GetWindow();
private:
	GLFWwindow* window = NULL;
	bool visible = true;
};

