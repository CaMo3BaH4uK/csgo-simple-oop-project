#include "GUI.h"

GUI::GUI()
{
    if (!glfwInit()) {
        assert(0, "Can't init glfw");
    }

    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, 1);
    glfwWindowHint(GLFW_FOCUS_ON_SHOW, 1);
    glfwWindowHint(GLFW_MOUSE_PASSTHROUGH, 0);
    glfwWindowHint(GLFW_FLOATING, 1);
    glfwWindowHint(GLFW_MAXIMIZED, 1);
    glfwWindowHint(GLFW_DECORATED, 0);

    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

    window = glfwCreateWindow(mode->width, mode->height, "Wednesday", 
        NULL, NULL);
    if (window == NULL) {
        assert(0, "Can't create window");
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
}

void GUI::Prepare()
{
    glfwPollEvents();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void GUI::Render()
{
    ImGui::Render();
    int display_w, display_h;
    glfwGetFramebufferSize(window, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(window);
}

void GUI::SetWindowPos(RECT lnPos)
{
    glfwSetWindowPos(window, lnPos.left, lnPos.top);
}

void GUI::SetWindowSize(std::pair<int, int> lnSize)
{
    glfwSetWindowSize(window, lnSize.first, lnSize.second);
}

void GUI::SetWindowMinimization(bool lnMinimize)
{
    if (lnMinimize) {
        glfwIconifyWindow(window);
    }
    else {
        glfwRestoreWindow(window);
    }
}

void GUI::SetVisible(bool lnVisible)
{
    this->visible = lnVisible;
    glfwWindowHint(GLFW_MOUSE_PASSTHROUGH, !lnVisible);
}

bool GUI::IsMinimized()
{
    return glfwGetWindowAttrib(window, GLFW_ICONIFIED);
}

bool GUI::IsVisible()
{
    return visible;
}

bool GUI::WindowShouldClose()
{
    return glfwWindowShouldClose(window);
}

GLFWwindow* GUI::GetWindow()
{
    return window;
}
