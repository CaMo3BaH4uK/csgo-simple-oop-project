#include "Memory.h"

Memory::Memory(const std::wstring &lnProcessName)
{
	::PROCESSENTRY32 entry = { };
	entry.dwSize = sizeof(::PROCESSENTRY32);

	const auto snapShot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	while (::Process32Next(snapShot, &entry)) {
		if (!lnProcessName.compare(entry.szExeFile)) {
			processId = entry.th32ProcessID;
			processHandle = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, 
				processId);
			break;
		}
	}

	if (snapShot)
		::CloseHandle(snapShot);

	assert(processId, "Can't open process");
}

Memory::~Memory()
{
	if (processHandle)
		::CloseHandle(processHandle);
}

const std::uintptr_t Memory::GetModuleAddress(const std::wstring lnModuleName)
{
	::MODULEENTRY32 entry = { };
	entry.dwSize = sizeof(::MODULEENTRY32);

	const auto snapShot = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, 
		processId);

	std::uintptr_t result = 0;

	while (::Module32Next(snapShot, &entry)) {
		if (!lnModuleName.compare(entry.szModule)) {
			result = reinterpret_cast<std::uintptr_t>(entry.modBaseAddr);
			break;
		}
	}

	if (snapShot)
		::CloseHandle(snapShot);

	assert(result, "Can't find modules");

	return result;
}

std::uintptr_t Memory::GetProcessId()
{
	return processId;
}

