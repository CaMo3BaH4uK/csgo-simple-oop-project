#pragma once

#include <string>
#include <Windows.h>
#include <TlHelp32.h>
#include <cstdint>
#include <assert.h>

class Memory {
public:
	Memory(const std::wstring &lnProcessName);
	~Memory();

	const std::uintptr_t GetModuleAddress(const std::wstring lnModuleName);

	std::uintptr_t GetProcessId();

	template <typename T>
	constexpr const T Read(const std::uintptr_t& lnAddress);

	template <typename T>
	constexpr void Write(const std::uintptr_t& lnAddress, const T& lnValue);

private:
	std::uintptr_t processId = 0;
	void* processHandle = nullptr;
};

template<typename T>
constexpr const T Memory::Read(const std::uintptr_t& lnAddress)
{
	T value = { };
	::ReadProcessMemory(processHandle, 
		reinterpret_cast<const void*>(lnAddress), &value, sizeof(T), NULL);
	return value;
}

template<typename T>
constexpr void Memory::Write(const std::uintptr_t& lnAddress, const T& lnValue)
{
	::WriteProcessMemory(processHandle, 
		reinterpret_cast<void*>(lnAddress), &lnValue, sizeof(T), NULL);
}
