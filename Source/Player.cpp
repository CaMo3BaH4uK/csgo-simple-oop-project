#include "Player.h"

Player::Player(Game* lnGame, uintptr_t lnAddress)
{
	game = lnGame;
	baseAddress = lnAddress;
}

void Player::SetGlow(ImVec4 lnColor, bool lnEnable)
{
	int glowIdx = GetGlowIdx();
	uintptr_t glowObjectManager = game->GetGlowObjectManager();
	game->Write<float>(glowObjectManager + (glowIdx * 0x38 + 0x8), lnColor.x);
	game->Write<float>(glowObjectManager + (glowIdx * 0x38 + 0xC), lnColor.y);
	game->Write<float>(glowObjectManager + (glowIdx * 0x38 + 0x10), lnColor.z);
	game->Write<float>(glowObjectManager + (glowIdx * 0x38 + 0x14), lnColor.w);
	game->Write<bool>(glowObjectManager + (glowIdx * 0x38 + 0x28), true);
	game->Write<bool>(glowObjectManager + (glowIdx * 0x38 + 0x29), false);
}

void Player::SetModelColor(ImVec4 lnColor)
{
	game->Write<uint8_t>(baseAddress + 0x70, (uint8_t)(255 * lnColor.x));
	game->Write<uint8_t>(baseAddress + 0x71, (uint8_t)(255 * lnColor.y));
	game->Write<uint8_t>(baseAddress + 0x72, (uint8_t)(255 * lnColor.z));
}

void Player::SetModelBrightness(float lnBrightness, bool lnEnable)
{
	if (!lnEnable) {
		lnBrightness = 0;
	}
	else {
		lnBrightness *= 10;
	}
	uintptr_t thisPtr = *(uintptr_t*)&lnBrightness ^ 
		(uintptr_t)(game->GetEngineAddress() + 
			hazedumper::signatures::model_ambient_min - 0x2c);
	game->Write<uintptr_t>(game->GetEngineAddress() + 
		hazedumper::signatures::model_ambient_min, thisPtr);
}

bool Player::IsCorrect()
{
	return baseAddress;
}

int Player::GetTeam()
{
	return game->Read<int>(baseAddress + hazedumper::netvars::m_iTeamNum);
}

int Player::GetHealth()
{
	return game->Read<int>(baseAddress + hazedumper::netvars::m_iHealth);
}

int Player::GetArmor()
{
	return game->Read<int>(baseAddress + hazedumper::netvars::m_ArmorValue);
}

int Player::GetGlowIdx()
{
	return game->Read<int>(baseAddress + hazedumper::netvars::m_iGlowIndex);
}

bool Player::HasHelmet()
{
	return game->Read<bool>(baseAddress + hazedumper::netvars::m_bHasHelmet);
}

bool Player::IsDefusing()
{
	return game->Read<bool>(baseAddress + hazedumper::netvars::m_bIsDefusing);
}

bool Player::HasDefuseKit()
{
	return game->Read<bool>(baseAddress + hazedumper::netvars::m_bHasDefuser);
}
