#pragma once

#include "Chams.h"
#include "Window.h"

class ChamsWindow : protected Chams, public Window {
public:
	ChamsWindow(Game* lnGame);

	void Draw();
};

