#pragma once

#include <string>
#include <map>

#include "GUI.h"
#include "Game.h"

#include "MenuWindow.h"
#include "GlowWindow.h"
#include "ChamsWindow.h"


class Engine {
public:
	Engine(std::wstring& lnProcessName);

	void Runtime();
private:
	Game game;
	GUI gui;

	MenuWindow menuWindow;
	GlowWindow glowWindow;
	ChamsWindow chamsWindow;

	std::map<std::string, bool*> menuItems{
		{"Glow ESP menu", &glowWindow.opened},
		{"Chams menu", &chamsWindow.opened}
	};
};

