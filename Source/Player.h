#pragma once

#include <memory>

#include "Game.h"
#include "Offsets.h"
#include "imgui.h"

class Player {
public:
	Player(Game* lnGame, uintptr_t lnAddress);

	void SetGlow(ImVec4 lnColor, bool lnEnable);
	void SetModelColor(ImVec4 lnColor);
	void SetModelBrightness(float lnBrightness, bool lnEnable);

	bool IsCorrect();

	int GetTeam();
	int GetHealth();
	int GetArmor();
	int GetGlowIdx();
	bool HasHelmet();
	bool IsDefusing();
	bool HasDefuseKit();

private:
	Game* game;
	uintptr_t baseAddress = NULL;
};

