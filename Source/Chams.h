#pragma once

#include "Game.h"
#include "Player.h"
#include "Offsets.h"
#include "imgui.h"

class Chams {
public:
	Chams(Game* lnGame);
	void Tick();

	bool chamsState = true;
	bool enableFriendly = true;
	ImVec4 enemyColor = {1,0,0,1};
	ImVec4 friendsColor = {0,1,0,1};
private:
	Game* game;
};

