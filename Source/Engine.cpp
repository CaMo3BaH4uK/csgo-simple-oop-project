#include "Engine.h"


Engine::Engine(std::wstring& lnProcessName) : game(lnProcessName), 
menuWindow(&gui, menuItems), glowWindow(&game), chamsWindow(&game)
{
}

void Engine::Runtime()
{
	while (!gui.WindowShouldClose()) {
		if (!game.IsMinimized()) {
			if (gui.IsMinimized() && menuWindow.IsOpened()) {
				gui.SetWindowMinimization(false);
			}
			RECT gamePos = game.GetPosition();
			std::pair<int, int> gameResolution = game.GetResolution();
			//gui.SetWindowPos(gamePos);
			//gui.SetWindowSize(gameResolution);
			gui.Prepare();

			menuWindow.Draw();
			glowWindow.Draw();
			chamsWindow.Draw();

			gui.Render();
		}
		else {
			gui.SetWindowMinimization(true);
		}
	}
}
