#include "Chams.h"

Chams::Chams(Game* lnGame)
{
	game = lnGame;
}

void Chams::Tick()
{
	Player localplayer(game, game->GetLocalPlayer());
	for (size_t i = 0; i < 64; i++) {
		Player player(game, game->GetPlayerByIdx(i));
		if (!player.IsCorrect()) {
			continue;
		}

		if (player.GetHealth() <= 0 || 
			(player.GetTeam() == localplayer.GetTeam() && 
				!enableFriendly)) {
			continue;
		}

		if (enableFriendly && player.GetTeam() == localplayer.GetTeam()) {
			player.SetModelColor(friendsColor);
			player.SetModelBrightness(friendsColor.w, chamsState);
		}
		else {
			player.SetModelColor(enemyColor);
			player.SetModelBrightness(enemyColor.w, chamsState);
		}
	}
}
