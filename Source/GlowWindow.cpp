#include "GlowWindow.h"

GlowWindow::GlowWindow(Game* lnGame) : Glow(lnGame)
{
}

void GlowWindow::Draw()
{
	Tick();

	if (opened) {
		ImGui::Begin("Glow ESP config", &opened);
		ImGui::Checkbox("Enable", &glowState);
		if (glowState) {
			ImGui::ColorEdit3("Enemy color", (float*)& enemyColor);
			ImGui::Checkbox("Show friendly",&enableFriendly);
			if (enableFriendly) {
				ImGui::ColorEdit3("Friendly color", (float*)&friendsColor);
			}
		}
		ImGui::End();
	}
}
