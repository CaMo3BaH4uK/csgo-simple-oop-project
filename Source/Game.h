#pragma once

#include <vector>
#include <assert.h>

#include "Memory.h"
#include "Offsets.h"

class Game : public Memory {
public:
	Game(const std::wstring& lnProcessName);
	std::pair<int, int> GetResolution();
	RECT GetPosition();
	bool IsMinimized();
	uintptr_t GetClientAddress();
	uintptr_t GetEngineAddress();

	uintptr_t GetPlayerByIdx(int idx);
	uintptr_t GetLocalPlayer();
	uintptr_t GetGlowObjectManager();
private:
	void GetAllGameWindows(std::vector <HWND>& lnHWNDs);
	HWND GetCorrectGameWindow(std::vector <HWND>& lnHWNDs);

	HWND gameWindow;
	uintptr_t client = NULL;
	uintptr_t engine = NULL;
};

