#include "Game.h"

Game::Game(const std::wstring &lnProcessName):Memory(lnProcessName)
{
    std::vector <HWND> HWNDs;
    GetAllGameWindows(HWNDs);
    gameWindow = GetCorrectGameWindow(HWNDs);
    client = GetModuleAddress(L"client.dll");
    engine = GetModuleAddress(L"engine.dll");
}

std::pair<int, int> Game::GetResolution()
{
    RECT rect = GetPosition();
    return std::pair<int, int>(abs(rect.left - rect.right), 
        abs(rect.top - rect.bottom));
}

RECT Game::GetPosition()
{
    RECT rect;
    GetClientRect(gameWindow, &rect);
    ClientToScreen(gameWindow, reinterpret_cast<POINT*>(&rect.left));
    ClientToScreen(gameWindow, reinterpret_cast<POINT*>(&rect.right));
    return rect;
}

bool Game::IsMinimized()
{
    return IsIconic(gameWindow);
}

uintptr_t Game::GetClientAddress()
{
    return client;
}

uintptr_t Game::GetEngineAddress()
{
    return engine;
}

uintptr_t Game::GetPlayerByIdx(int idx)
{
    return Read<uintptr_t>(client + hazedumper::signatures::dwEntityList + 
        idx * 0x10);
}

uintptr_t Game::GetLocalPlayer()
{
    return Read<uintptr_t>(client + hazedumper::signatures::dwLocalPlayer);
}

uintptr_t Game::GetGlowObjectManager()
{
    return Read<uintptr_t>(client + 
        hazedumper::signatures::dwGlowObjectManager);
}

void Game::GetAllGameWindows(std::vector<HWND>& lnHWNDs)
{
    HWND hCurWnd = NULL;
    do {
        hCurWnd = FindWindowEx(NULL, hCurWnd, NULL, NULL);
        DWORD dwProcID = 0;
        GetWindowThreadProcessId(hCurWnd, &dwProcID);
        if (dwProcID == GetProcessId() && IsWindow(hCurWnd)) {
            lnHWNDs.push_back(hCurWnd);
        }
    } while (hCurWnd != NULL);
}

HWND Game::GetCorrectGameWindow(std::vector<HWND>& lnHWNDs)
{
    HWND hwnd = NULL;
    for (HWND i : lnHWNDs) {
        RECT rect;
        GetWindowRect(i, &rect);
        if (rect.right) {
            hwnd = i;
            break;
        }
    }
    assert(hwnd != NULL, "Can't find correct game window");
    return hwnd;
}
