#include "MenuWindow.h"

MenuWindow::MenuWindow(GUI* lnGUI, std::map<std::string, bool*> &lnMenuItems) :
	menuItems(lnMenuItems), gui(lnGUI)
{
}

void MenuWindow::Draw()
{
	if (GetAsyncKeyState(VK_END)) {
		opened = !opened;
		Sleep(150);
	}

	if (gui->IsMinimized() == opened) {
		gui->SetVisible(opened);
		gui->SetWindowMinimization(!opened);
	}

	if (opened) {
		ImGui::Begin("Main menu", &opened);
		for (auto const& [key, val] : menuItems) {
			ImGui::Checkbox(key.c_str(), val);
		}
		ImGui::End();
	}
}
