#include "ChamsWindow.h"

ChamsWindow::ChamsWindow(Game* lnGame) : Chams(lnGame)
{
}

void ChamsWindow::Draw()
{
	Tick();

	if (opened) {
		ImGui::Begin("Chams config", &opened);
		ImGui::Checkbox("Enable", &chamsState);
		if (chamsState) {
			ImGui::ColorEdit3("Enemy color", (float*)&enemyColor);
			ImGui::Checkbox("Show friendly", &enableFriendly);
			if (enableFriendly) {
				ImGui::ColorEdit3("Friendly color", (float*)&friendsColor);
			}
		}
		ImGui::End();
	}
}
