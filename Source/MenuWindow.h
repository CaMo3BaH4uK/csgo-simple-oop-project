#pragma once

#include <map>
#include <string>
#include <Windows.h>

#include "GUI.h"
#include "Window.h"

class MenuWindow : public Window {
public:
	MenuWindow(GUI* lnGUI, std::map<std::string, bool*> &lnMenuItems);
	void Draw();
private:
	std::map<std::string, bool*>& menuItems;
	GUI* gui;
};

