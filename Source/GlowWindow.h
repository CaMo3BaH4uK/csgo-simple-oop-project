#pragma once

#include "Glow.h"
#include "Window.h"

class GlowWindow : protected Glow, public Window {
public:
	GlowWindow(Game* lnGame);

	void Draw();
};

