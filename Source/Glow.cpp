#include "Glow.h"

#include <iostream>

Glow::Glow(Game* lnGame)
{
	game = lnGame;
}

void Glow::Tick()
{
	if (glowState) {
		uintptr_t glowManager = game->GetGlowObjectManager();

		Player localplayer(game, game->GetLocalPlayer());

		for (size_t i = 0; i < 64; i++) {
			Player player(game, game->GetPlayerByIdx(i));

			if (!player.IsCorrect()) {
				continue;
			}

			if (player.GetHealth() <= 0 || 
				(player.GetTeam() == localplayer.GetTeam() && 
					!enableFriendly)) {
				continue;
			}

			if (enableFriendly && player.GetTeam() == localplayer.GetTeam()) {
				player.SetGlow(friendsColor, glowState);
			}
			else {
				player.SetGlow(enemyColor, glowState);
			}
		}
	}
}
